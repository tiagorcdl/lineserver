# lineserver

Some helpful commands


## Building
  ```
  build.sh
  ```
## Running
  ```
  run.sh <file path>
  ```
## Calling the application

* get a line 
  ```
  curl -v localhost:8080/lines/<line number (starting at 0)>
  ```

# program details

---

#### How does your system work? (if not addressed in comments in source)
    
On start up a pre-processing step is executed creating an index file in the systems temp 
directory containing the position index of all lines (first character).

This makes use of the known character encoding. 

With this, it is possible to skip `Long.BYTES * line` number of bytes forward to then read the line.
This pre-processing step creates a singleton indexed file that will be used by every request.

The indexes are stored in a temporary file as a sequence of `longs`. 
The accesses to this file are cached to avoid excessive file system usage, 
and the cache size is configurable.

To retrieve the line, a RandomAccessFile is used to seek to the desired file position and read a 
complete line.

---
#### How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file? 

The pre-processing step was added to avoid performance issues while serving large files. 

A file is used to store the indexes of all lines to isolate used memory from file size.
The tradeoff is time. In my tests, the pre-processing step takes around a second for a 10 MB file, 
and around a minute for a 20 GB one.

The total memory used to process the file is constant, due to the use of input and output buffered streams, 
while reading line by line and writing indexes to file.

---
#### How will your system perform with 100 users? 10000 users? 1000000 users?  

I've used a lightweight web framework [jooby][jooby] to deal with the server aspect
of handling multiple clients. 

To avoid accessing the indexation file on every request, I've added a [cache][cache2k]. 
The size (number of entries) of this cache is defined by a property, allowing, at deployment time, 
to choose a tradeoff between memory used and hits on the file system.
If one makes the cache size equal to the number of lines in the file, the first request for each 
line will hit the file, but every other request will be cached. The cache is shared by all threads.

Caching the lines themselves seemed too memory expensive, but with 1000000 
requests, the file system might not handle the load with optimal response time. 
In that case, a load balancer could be used to have multiple machines responding, 
or pay the memory price and add a response cache. 

Making this a future work, allows for some analyzes on traffic patterns to identify optimization 
strategies for this heavier cache.

---
#### What documentation, websites, papers, etc did you consult in doing this assignment? 

* File operations
  * [Articles: Tuning Java I/O Performance][art-java-io]
  * [High-Permormance File I/O in Java: Existing Approaches and Bulk I/O Extensions][paper-io-in-java]
* Framework
  * [jooby][jooby]
* Libs
  * [vavr.try][vavr-try]
  * [vavr.either][vavr-either]
  * [cache2k][cache2k]
    * [caching benchmarks][caching-benchmarks]
* Documentation
  * [markdown reference][markdown]
* Unfortunately I didn't keep good track of all stackoverflow threads 
I've read while doing the assignment.

---
#### What third-party libraries or other tools does the system use? How did you choose each library or framework you used? 
- jooby2 (framework)
  - lightweight
  - familiar
- vavr
  - avoiding exception base control flow
- cache2k
  - overall best performer in a comparison a bit old ([caching benchmarks][caching-benchmarks])
- testing (all used because they were familiar)
  - JUnit5
  - Mockito

---
#### How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item? 

I've spent around 15h. 

With more time I could improve the following topics:
- Adding metrics to enable optimization based on usage. 
- More tests to cover edge cases. 
- This README could be improved as well.
 
---
#### If you were to critique your code, what would you have to say about it?

I would have used a version control system from the start. It would have been a good 
helper along the way and I would love to see my own progress.

It's always possible to make it better. I've spent too much time working small details that 
ended up being unnecessary and deleted. 
I am happy with the solution, but it would for sure benefit from input from others 
(brainstorming and code review).

[caching-benchmarks]: https://cruftex.net/2017/09/01/Java-Caching-Benchmarks-Part-3.html
[art-java-io]: https://www.oracle.com/technical-resources/articles/javase/perftuning.html
[paper-io-in-java]: https://www.mcs.anl.gov/~thakur/papers/javaio-journal.pdf
[jooby]: https://jooby.io/
[vavr-try]: https://docs.vavr.io/#_try
[vavr-either]: https://docs.vavr.io/#_either
[cache2k]: https://cache2k.org/docs/latest/user-guide.html#getting-started
[markdown]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
