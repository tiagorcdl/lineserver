#!/bin/bash

if [ $# -ne 1 ]; then
    echo -e "\nUsage: $0 <filePath> \n";
    exit 1;
fi;

java -jar target/lineserver-1.0.0.jar file.path="$1" application.env=prod;

exit 0;