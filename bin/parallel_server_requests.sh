#!/bin/bash

if [ $# -ne 4 ]; then
    echo -e "\nUsage: $0 <number of threads> <max line number> <number of requests per thread> <sleep (in seconds) between calls> \n\n";
    exit 1;
fi;

NUMBER_OF_THREADS=$1
MAX_INDEX=$2
NUMBER_ITERATIONS=$3;
SLEEP_BETWEEN_CALLS=$4;

function cleanUp() {
    pkill -P $$;
}
trap cleanUp EXIT;

function foo() {
  NUMBER_ITERATIONS=$1;
  SLEEP_BETWEEN_CALLS=$2;
  MAX_INDEX=$3;

  for i in $(seq 0 "$(("$NUMBER_ITERATIONS" - 1))");
  do
    curl -o /dev/null -s -w "%{http_code}\n" "localhost:8080/lines/$(shuf -i 1-$MAX_INDEX -n 1)";
  done;

}

for i in $(seq 0 "$(("$NUMBER_OF_THREADS" - 1))");
do
  foo  "$NUMBER_ITERATIONS" "$SLEEP_BETWEEN_CALLS" "$MAX_INDEX" &
done;


sleep "$(echo "$NUMBER_ITERATIONS*$SLEEP_BETWEEN_CALLS+1" | bc)";

exit 0;