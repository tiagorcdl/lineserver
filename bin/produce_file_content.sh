#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "\nUsage: $0 <number of lines> <Number of repetitions of the string> \n\n";
    exit 1;
fi;

NUMBER_OF_LINES=$1;
NUMBER_OF_REPETITIONS=$2;

REPEATED_STRING="some words that will be repeated. ";
FINAL_STRING="";

for i in $(seq 0 "$(("NUMBER_OF_REPETITIONS" - 1))");
do FINAL_STRING+=$REPEATED_STRING;
done;

for i in $(seq 0 "$(("$NUMBER_OF_LINES" - 1))");
do echo "$i. $FINAL_STRING";
done;

exit 0;




