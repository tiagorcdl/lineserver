package app.common;

public abstract class ApplicationError {

	private final String message;

	protected ApplicationError(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
