package app.fileindexation;

import com.typesafe.config.Config;
import io.jooby.Extension;
import io.jooby.Jooby;
import io.jooby.ServiceRegistry;

public class IndexFileExtension implements Extension {

	@Override
	public void install(final Jooby application) {
		final Config conf = application.getEnvironment().getConfig();

		final ServiceRegistry registry = application.getServices();

		new IndexedFileImpl.Builder()
			.withFilePath(conf.getString("file.path"))
			.withCacheCapacity(conf.getLong("cache.lineIndex.capacity"))
			.build()
			.map(indexedFile -> registry.put(
				IndexedFile.class,
				indexedFile))
			.getOrElseThrow(someError -> new FileIndexationFailure(someError.getMessage()));

		application.onStop(() -> {
			final IndexedFile indexedFile = registry.get(IndexedFile.class);
			indexedFile.close();
		});

	}
}
