package app.fileindexation;

import java.io.File;
import java.io.IOException;

public interface IndexedFile {

	File getSubjectFile();

	Long getLinePositionIndex(int index);

	void close() throws IOException;
}
