package app.fileindexation;

import app.util.ByteUtils;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.cache2k.addon.UniversalResiliencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexedFileImpl implements Closeable, IndexedFile {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndexedFileImpl.class);

	private final File subjectFile;
	private final File indexFile;
	private final Cache<Integer, Long> cache;


	private IndexedFileImpl(final File subjectFile, final File indexFile, final Cache<Integer, Long> cache) {
		this.subjectFile = subjectFile;
		this.indexFile = indexFile;
		this.cache = cache;
	}

	public File getSubjectFile() {
		return subjectFile;
	}

	public Long getLinePositionIndex(int index) {
		return this.cache.get(index);
	}

	@Override
	public void close() throws IOException {
		Files.delete(Paths.get(this.indexFile.getPath()));
		cache.close();
	}

	public static class Builder {
		public static final int DEFAULT_CACHE_SIZE = 10;
		private Long cacheCapacity;
		private String filePath;

		public Builder() {
			//Nothing to be set in here
		}

		public Builder withCacheCapacity(final long cacheCapacity) {
			this.cacheCapacity = cacheCapacity;
			return this;
		}

		public Builder withFilePath(final String filePath) {
			this.filePath = filePath;
			return this;
		}

		public Either<FileIndexationError, IndexedFile> build() {
			if (this.filePath == null) {
				return Either.left(FileIndexationError.documentNotFoundError("Missing file path"));
			}

			LOGGER.info("Indexing file: {}", this.filePath);
			final File file = new File(this.filePath);

			return Try
				.of(() -> File.createTempFile(file.getName(), "cache"))
				.toEither()
				.mapLeft(throwable -> FileIndexationError.ioOperationError("Could no create indexation file"))
				.map(cacheFile -> Try
					.withResources(
						() -> new BufferedReader(new InputStreamReader(
							new FileInputStream(file),
							StandardCharsets.US_ASCII)),
						() -> new BufferedOutputStream(new FileOutputStream(cacheFile)))
					.of((bufferedReader, bufferedOutputStream) -> withSubjectAndOutputFilesGenerateIndexFile(
						file,
						cacheFile,
						bufferedReader,
						bufferedOutputStream,
						this.cacheCapacity == null ? DEFAULT_CACHE_SIZE : this.cacheCapacity ))
					.getOrElseGet(throwable -> {
						try {
							Files.delete(Paths.get(cacheFile.getPath()));
						} catch (IOException e) {
							//best effort
						}
						return Builder.handleExceptions(throwable);
					}))
				.getOrElseGet(Either::left);
		}

		private static Either<FileIndexationError, IndexedFile> withSubjectAndOutputFilesGenerateIndexFile(
			final File file,
			final File cacheFile,
			final BufferedReader bufferedReader,
			final BufferedOutputStream bufferedOutputStream,
			final long cacheCapacity) throws IOException
		{
			long position = 0;
			String line;
			bufferedOutputStream.write(ByteUtils.longToBytes(position));
			while ((line = bufferedReader.readLine()) != null) {
				final byte[] lineBytes = line.getBytes(StandardCharsets.US_ASCII);
				position += lineBytes.length + 1;
				bufferedOutputStream.write(ByteUtils.longToBytes(position));
			}

			Cache<Integer, Long> cache = setupCache(cacheFile, cacheCapacity);

			return Either.right(new IndexedFileImpl(file, cacheFile, cache));
		}

		private static Cache<Integer, Long> setupCache(final File cacheFile, final long cacheCapacity) {
			return Cache2kBuilder
				.of(Integer.class, Long.class)
				.name("FileIndexCache")
				.entryCapacity(cacheCapacity)
				.loader(index -> {
					try (final BufferedInputStream in = new BufferedInputStream(new FileInputStream(cacheFile))) {
						byte[] b = new byte[Long.BYTES];
						final int bytesToSkip = index * Long.BYTES;
						if (in.skip(bytesToSkip) == bytesToSkip && in.read(b, 0, Long.BYTES) > 0) {
							return ByteUtils.bytesToLong(b);
						} else {
							return -1L;
						}
					}
				})
				.eternal(true)
				.setupWith(
					UniversalResiliencePolicy::enable,
					builder -> builder.retryInterval(10, TimeUnit.SECONDS))
				.build();
		}

		private static Either<FileIndexationError, IndexedFile> handleExceptions(final Throwable throwable) {
			if (throwable instanceof FileNotFoundException) {
				return Either.left(FileIndexationError.documentNotFoundError("File not found"));
			}
			return Either.left(FileIndexationError.ioOperationError(throwable.getMessage()));
		}

	}

}
