package app.fileindexation;

public class FileIndexationFailure extends RuntimeException{

	FileIndexationFailure(final String message) {
		super(message);
	}

}
