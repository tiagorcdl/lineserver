package app.fileindexation;

import app.common.ApplicationError;

public abstract class FileIndexationError extends ApplicationError {

	private FileIndexationError(final String message) {
		super(message);
	}

	static FileIndexationError documentNotFoundError(final String message) {
		return new DocumentNotFoundError(message);
	}

	static FileIndexationError ioOperationError(final String message) {
		return new IOOperationError(message);
	}

	public static final class DocumentNotFoundError extends FileIndexationError {
		private DocumentNotFoundError(final String message) {
			super(message);
		}

	}

	public static final class IOOperationError extends FileIndexationError {
		private IOOperationError(final String message) {
			super(message);
		}

	}

}
