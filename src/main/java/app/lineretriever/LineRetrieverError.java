package app.lineretriever;

import app.common.ApplicationError;

public abstract class LineRetrieverError extends ApplicationError {

	private LineRetrieverError(final String message) {
		super(message);
	}

	public abstract <T> T visit(LineServiceErrorVisitor<T> visitor);

	static IndexOutOfBounds indexOutOfBounds(final String message) {
		return new IndexOutOfBounds(message);
	}

	static IOOperationError ioOperationError(final String message) {
		return new IOOperationError(message);
	}

	public static final class IndexOutOfBounds extends LineRetrieverError {
		private IndexOutOfBounds(final String message) {
			super(message);
		}

		@Override
		public <T> T visit(final LineServiceErrorVisitor<T> visitor) {
			return visitor.visit(this);
		}
	}

	public static final class IOOperationError extends LineRetrieverError {
		private IOOperationError(final String message) {
			super(message);
		}

		@Override
		public <T> T visit(final LineServiceErrorVisitor<T> visitor) {
			return visitor.visit(this);
		}
	}

	public interface LineServiceErrorVisitor<T>{

		T visit(IndexOutOfBounds indexOutOfBounds);
		T visit(IOOperationError ioOperationError);
	}
}
