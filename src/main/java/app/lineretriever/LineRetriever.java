package app.lineretriever;

import app.fileindexation.IndexedFile;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class LineRetriever {

	private LineRetriever() {
		//Hide implicit public constructor
	}

	public static Either<LineRetrieverError, String> getLine(final IndexedFile indexedFile, final int index) {
		final Long lineIndex = indexedFile.getLinePositionIndex(index);
		if (lineIndex<0) {
			return Either.left(indexBeyondMaxLineCount(index));
		}
		return withLineIndexRetrieveLine(indexedFile.getSubjectFile(), index, lineIndex);
	}

	private static Either<LineRetrieverError, String> withLineIndexRetrieveLine(
		final File subjectFile,
		final int index,
		final Long lineIndex)
	{
		return Try.withResources(() ->  new RandomAccessFile(subjectFile, "r"))
				  .of(file -> withSubjectFileGetLine(index, lineIndex, file))
				  .getOrElseGet(ex -> Either.left(LineRetriever.handleException(ex)));
	}

	private static Either<LineRetrieverError, String> withSubjectFileGetLine(
		final int index,
		final Long lineIndex,
		final RandomAccessFile file) throws IOException
	{
		file.seek(lineIndex);
		final String line = file.readLine();
		if (line != null) {
			return Either.right(line);
		}
		return Either.left(indexBeyondMaxLineCount(index));
	}

	private static LineRetrieverError handleException(final Throwable throwable) {
		return LineRetrieverError.ioOperationError(throwable.getMessage());
	}

	private static LineRetrieverError indexBeyondMaxLineCount(final int index) {
		return LineRetrieverError.indexOutOfBounds(String.format("Index %s beyond max line count", index));
	}
}
