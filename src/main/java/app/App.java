package app;

import app.fileindexation.IndexFileExtension;
import app.fileindexation.IndexedFile;
import app.lineretriever.LineRetriever;
import app.lineretriever.LineRetrieverError.IOOperationError;
import app.lineretriever.LineRetrieverError.IndexOutOfBounds;
import app.lineretriever.LineRetrieverError.LineServiceErrorVisitor;
import app.util.SimpleMetrics;
import io.jooby.Context;
import io.jooby.Jooby;
import io.jooby.StatusCode;
import java.time.Instant;
import org.slf4j.Logger;

public class App extends Jooby {

	{
		final Logger logger = getLog();

		SimpleMetrics.withMetrics(
			logger,
			() -> install(new IndexFileExtension()));

		decorator(next -> ctx -> {
			final Instant startingInstant = Instant.now();
			final Object response = next.apply(ctx);
			SimpleMetrics.logDuration(logger, startingInstant);
			SimpleMetrics.logUsedMemory(logger);
			return response;
		});

		get("/lines/{index:[0-9]+}", ctx -> {
			int index = ctx.path("index").intValue();

			final IndexedFile indexedFile = require(IndexedFile.class);
			return LineRetriever
				.getLine(indexedFile, index)
				.fold(lineServiceError -> lineServiceError.visit(new LineServiceErrorVisitor<Context>() {

					@Override
					public Context visit(final IndexOutOfBounds indexOutOfBounds) {
						return ctx.send(StatusCode.REQUEST_ENTITY_TOO_LARGE);
					}

					@Override
					public Context visit(final IOOperationError ioOperationError) {
						logger.error(ioOperationError.getMessage());
						return ctx.send(StatusCode.SERVER_ERROR);
					}
				}), ctx::send);
		});

		error(StatusCode.NOT_FOUND, (ctx, cause, statusCode) -> ctx.send(statusCode));
	}

	public static void main(final String[] args) {
		runApp(args, App::new);
	}

}
