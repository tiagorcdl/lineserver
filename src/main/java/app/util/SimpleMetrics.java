package app.util;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Supplier;
import org.slf4j.Logger;

public interface SimpleMetrics {

	static <T> T withMetrics(final Logger logger, Supplier<T> fn) {
		logUsedMemory(logger);
		final Instant starting = Instant.now();
		final T result = fn.get();
		logDuration(logger, starting);
		logUsedMemory(logger);
		return result;
	}

	static void logDuration(final Logger logger, final Instant start) {
		if (logger.isDebugEnabled()) {
			logger.info("Duration: {}", Duration.between(start, Instant.now()));
		}
	}

	static void logUsedMemory(final Logger logger) {
		if (logger.isDebugEnabled()) {
			logger.info(
				"MB: {}",
				(double) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 / 1024);
		}
	}
}
