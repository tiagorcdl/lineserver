package app.lineretriever;

import app.fileindexation.IndexedFile;
import io.vavr.control.Either;
import java.io.File;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class LineRetrieverTest {

	@Test
	void testNegativeIndex() {
		final int index = 1000;

		final IndexedFile indexFile = Mockito.mock(IndexedFile.class);
		Mockito.when(indexFile.getLinePositionIndex(index)).thenReturn(-1L);

		final Either<LineRetrieverError, String> result = LineRetriever.getLine(indexFile, index);

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals("Index 1000 beyond max line count", result.getLeft().getMessage());

	}

	@Test
	void testWrongSubjectFile() {
		final int index = 0;

		final IndexedFile indexFile = Mockito.mock(IndexedFile.class);
		Mockito.when(indexFile.getLinePositionIndex(index)).thenReturn(0L);
		Mockito.when(indexFile.getSubjectFile()).thenReturn(new File(">>>>>>>"));

		final Either<LineRetrieverError, String> result = LineRetriever.getLine(indexFile, index);

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals(">>>>>>> (No such file or directory)", result.getLeft().getMessage());

	}

	@Test
	void testWithExistingFile() {
		final int index = 0;

		final IndexedFile indexFile = Mockito.mock(IndexedFile.class);
		Mockito.when(indexFile.getLinePositionIndex(index)).thenReturn(0L);
		Mockito.when(indexFile.getSubjectFile()).thenReturn(new File("src/test/resources/test_file.txt"));

		final Either<LineRetrieverError, String> result = LineRetriever.getLine(indexFile, index);

		Assertions.assertTrue(result.isRight());
		Assertions.assertEquals(
			"0. some words that will be repeated. some words that will be repeated.",
			result.get());
	}

	@Test
	void testWithPositionIndexBeyondEndOfFile() {
		final int index = 10;

		final IndexedFile indexFile = Mockito.mock(IndexedFile.class);
		Mockito.when(indexFile.getLinePositionIndex(index)).thenReturn(300L);
		Mockito.when(indexFile.getSubjectFile()).thenReturn(new File("src/test/resources/test_file.txt"));

		final Either<LineRetrieverError, String> result = LineRetriever.getLine(indexFile, index);

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals("Index 10 beyond max line count", result.getLeft().getMessage());
	}

}