package app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.jooby.JoobyTest;
import io.jooby.StatusCode;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.jupiter.api.Test;

@JoobyTest(App.class)
class MalformedRequestIntegrationTest {

  static OkHttpClient client = new OkHttpClient();

  @Test
  void indexNotAnInt(int serverPort) throws IOException {
    Request req = new Request.Builder()
        .url("http://localhost:" + serverPort + "/lines/a")
        .build();

    try (Response rsp = client.newCall(req).execute()) {
      assertEquals("", rsp.body().string());
      assertEquals(StatusCode.NOT_FOUND.value(), rsp.code());
    }
  }

  @Test
  void indexNotAPositiveInt(int serverPort) throws IOException {
    Request req = new Request.Builder()
        .url("http://localhost:" + serverPort + "/lines/-1")
        .build();

    try (Response rsp = client.newCall(req).execute()) {
      assertEquals("", rsp.body().string());
      assertEquals(StatusCode.NOT_FOUND.value(), rsp.code());
    }
  }

}
