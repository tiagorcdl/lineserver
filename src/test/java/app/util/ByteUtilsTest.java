package app.util;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ByteUtilsTest {

	@Test
	void testLongToBytes() {
		final byte[] result = ByteUtils.longToBytes(1000000000000000L);

		Assertions.assertEquals(Long.BYTES, result.length);
		byte[] testArray = new byte[] {
			(byte) 0, (byte) 3,(byte) -115, (byte)126, (byte) -92, (byte) -58,(byte) -128, (byte)0
		};

		Assertions.assertEquals(testArray.length, result.length);

		for(int i = 0; i<testArray.length; i++) {
			Assertions.assertEquals(testArray[i], result[i]);
		}
	}

	@Test
	void testBytesToLong() {
		byte[] testArray = new byte[] {
			(byte) 0, (byte) 3,(byte) -115, (byte)126, (byte) -92, (byte) -58,(byte) -128, (byte)0
		};
		final long result = ByteUtils.bytesToLong(testArray);

		Assertions.assertEquals(1000000000000000L, result);
	}

	@Test
	void testBytesToLongWithToManyBytes() {
		byte[] testArray = new byte[] {
			(byte) 0, (byte) 3,(byte) -115, (byte)126, (byte) -92, (byte) -58,(byte) -128, (byte)0, (byte)0
		};
		Assertions.assertThrows(BufferOverflowException.class, () ->ByteUtils.bytesToLong(testArray));
	}

	@Test
	void testBytesToLongWithTooFewBytes() {
		byte[] testArray = new byte[] {
			(byte) 0, (byte) 3,(byte) -115, (byte)126, (byte) -92, (byte) -58,(byte) -128
		};
		Assertions.assertThrows(BufferUnderflowException.class, () ->ByteUtils.bytesToLong(testArray));
	}

}