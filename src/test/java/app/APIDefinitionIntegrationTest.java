package app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.jooby.JoobyTest;
import io.jooby.StatusCode;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.jupiter.api.Test;

@JoobyTest(App.class)
class APIDefinitionIntegrationTest {

  static OkHttpClient client = new OkHttpClient();

  @Test
  void successfullyRetrieveLine(int serverPort) throws IOException {
    Request req = new Request.Builder()
        .url("http://localhost:" + serverPort + "/lines/0")
        .build();

    try (Response rsp = client.newCall(req).execute()) {
      assertEquals("0. some words that will be repeated. some words that will be repeated.", rsp.body().string());
      assertEquals(StatusCode.OK.value(), rsp.code());
    }
  }

  @Test
  void lineBeyondEndOfFile(int serverPort) throws IOException {
    Request req = new Request.Builder()
        .url("http://localhost:" + serverPort + "/lines/3")
        .build();

    try (Response rsp = client.newCall(req).execute()) {
      assertEquals("", rsp.body().string());
      assertEquals(StatusCode.REQUEST_ENTITY_TOO_LARGE.value(), rsp.code());
    }
  }

  @Test
  void lineBeyondEndOfFileByBigMargin(int serverPort) throws IOException {
    Request req = new Request.Builder()
        .url("http://localhost:" + serverPort + "/lines/100")
        .build();

    try (Response rsp = client.newCall(req).execute()) {
      assertEquals("", rsp.body().string());
      assertEquals(StatusCode.REQUEST_ENTITY_TOO_LARGE.value(), rsp.code());
    }
  }
}
