package app.fileindexation;

import app.fileindexation.IndexedFileImpl.Builder;
import io.vavr.control.Either;
import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IndexedFileImplBuilderTest {

	@Test
	void testWithoutFilePath() {
		final Either<FileIndexationError, IndexedFile> result = new Builder().build();

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals("Missing file path", result.getLeft().getMessage());
	}

	@Test
	void testEmptyFilePath() {
		final Either<FileIndexationError, IndexedFile> result = new Builder().withFilePath("").build();

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals("Could no create indexation file", result.getLeft().getMessage());
	}

	@Test
	void testNotExistingFileInFilePath() {
		final Either<FileIndexationError, IndexedFile> result = new Builder().withFilePath("<<<<<").build();

		Assertions.assertTrue(result.isLeft());
		Assertions.assertEquals("File not found", result.getLeft().getMessage());
	}

	@Test
	void testWithFilePath() {
		final String path = "src/test/resources/test_file.txt";
		final Either<FileIndexationError, IndexedFile> result = new Builder().withFilePath(path).build();

		Assertions.assertTrue(result.isRight());
		final IndexedFile indexedFile = result.get();
		Assertions.assertEquals(0L, indexedFile.getLinePositionIndex(0));
		Assertions.assertEquals(71L, indexedFile.getLinePositionIndex(1));
		Assertions.assertEquals(142L, indexedFile.getLinePositionIndex(2));
		Assertions.assertEquals(213L, indexedFile.getLinePositionIndex(3));
		Assertions.assertEquals(-1L, indexedFile.getLinePositionIndex(4));

		Assertions.assertEquals(path, indexedFile.getSubjectFile().getPath());

		//Clean Up
		try {
			indexedFile.close();
		} catch (IOException e) {
			//Nothing to do
		}

	}

}