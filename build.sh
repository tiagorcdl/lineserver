#!/bin/bash

if ! command -v mvn &> /dev/null
then
    echo "maven not found. Please install and run again";
    exit 1;
fi

#Build
mvn clean package;
exit 0;